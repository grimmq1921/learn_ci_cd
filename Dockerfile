FROM python:3.11.0-alpine3.16

WORKDIR /usr/src/app

RUN pip install pytest

COPY *.py /usr/src/app/


CMD ["pytest"]
